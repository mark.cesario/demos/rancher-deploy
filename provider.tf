provider "aws" {
  region  = "us-west-2"
  profile = "default"
  access_key      = lookup(var.aws_access_keys, "access_key")
  secret_key      = lookup(var.aws_access_keys, "secret_key")

}

provider "rke" {
}

provider "helm" {
  kubernetes {
    config_path = local_file.kube_cluster_yaml.filename
  }
}

provider "rancher2" {
  version = "1.8.1"
  alias     = "bootstrap"
  api_url   = "https://${local.name}.${local.domain}"
  bootstrap = true
}

provider "rancher2" {
  version = "1.8.1"
  api_url   = "https://${local.name}.${local.domain}"
  token_key = rancher2_bootstrap.admin.token
}
